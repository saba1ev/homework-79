const fs = require('fs');

const filename = 'db.json';

let data = {};

module.exports = {
  init(){
    try {
      const fileContents = fs.readFileSync(filename);
      data = JSON.parse(fileContents);
    } catch (e) {
      data = {};
    }
  },
  getInfo(collactionName){
    return data[collactionName] || [];
  },
  addInfo(collactionName, info){
    if (!collactionName){
      data[collactionName] = [];
    }

    data[collactionName].push(info);
    save();
  },
  save(){
    fs.readFileSync(filename, JSON.stringify(data, null, 2));
  }
};