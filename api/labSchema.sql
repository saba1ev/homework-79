CREATE SCHEMA `labSchema` DEFAULT CHARACTER SET utf8 ;

USE `labSchema` ;

CREATE TABLE `categories` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL,
  `description` TEXT NULL,
  PRIMARY KEY (`id`));
CREATE TABLE `places` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL,
  `description` VARCHAR(45) NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `items` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `categories_id` INT NULL,
  `places_id` INT NULL,
  `title` VARCHAR(255) NOT NULL,
  `description` TEXT NULL,
  `image` VARCHAR(255) NULL,
  PRIMARY KEY (`id`),
  INDEX `places_id_idx` (`places_id` ASC),
  INDEX `categories_id_idx` (`categories_id` ASC),
  CONSTRAINT `categories_id`
    FOREIGN KEY (`categories_id`)
    REFERENCES `categories` (`id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE,
  CONSTRAINT `places_id`
    FOREIGN KEY (`places_id`)
    REFERENCES `places` (`id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE);

INSERT INTO `categories` (`id`, `title`, `description`)
VALUES
	(1, 'Мебель', 'some description'),
    (2, 'Техника', 'some description'),
    (3, 'Цветы', 'some description')
    ;
INSERT INTO `places` (`id`, `title`)
VALUES
	('1', 'lab-204'),
    ('2', 'lab-12'),
    ('3', 'room-47')
	;
INSERT INTO `items` (`id`, `categories_id`,
	`places_id`, `title`, `description`)
VALUES
	(1, 1, 2, 'Стул', 'это стул'),
	(2, 2, 1, 'Ноут', 'Это ноут'),
	(3, 3, 3, 'fialka', 'this is fialka')
    ;