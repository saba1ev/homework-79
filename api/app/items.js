const express = require('express');
const multer = require('multer');
const path = require('path');
const config = require('../config');
const nanoid = require('nanoid');
const db = require('../fileDb');

const storage = multer.diskStorage({
  destination: (req, file, cb)=>{
    cb(null, config.uploadPath)
  },
  filename: (req, file, cb)=>{
    cb(null, nanoid() + path.extname(file.originalname))
  }
});

const upload = multer({storage});

const createRouter = connection =>{
  const router = express.Router();

  router.get('/', (req, res)=>{
    connection.query('SELECT * FROM `items`', (error, result)=>{
      if (error){
        res.status(500).send({error: 'Database error'});
      }
      res.send(result);
    });
  });
  router.get('/:id', (req, res)=>{
    connection.query('SELECT * FROM `items` WHERE = ?', req.match.patams, (error, result) =>{
      if (error){
        res.status(500).send({error: 'Database error'});
      }
      if (result[0]){
        res.send(result[0]);
      } else {
        res.status(404).send({error:'Item not found'});
      }
    })
  });

  router.post('/', upload.single('image'), (req, res)=>{
    const item = req.body;
    if (req.file){
      item.image = req.file.filename
    }
    connection.query('INSERT TO `items` (`title`, `description`, `image`) VALUES (?,?,?)',
      [item.title, item.description, item.image],
      (error, result)=>{
        if (error){
          res.status(500).send({error: 'Database error'})
        }
        res.send({message: 'OK'})
      });
  });

  return router;
};



module.exports = createRouter;