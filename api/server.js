const express = require('express');
const cors = require('cors');
const items = require('./app/items');
const mysql = require('mysql');


const app = express();

app.use(cors());
app.use(express.json());
app.use(express.static('public'));

const port = 8000;

const connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'user',
  password : '1234',
  database : 'labSchema'
});

app.use('/items', items(connection));

connection.connect((err) => {
  if (err) {
    console.error('error connecting: ' + err.stack);
    return;
  }

  console.log('connected as id ' + connection.threadId);
  app.listen(port, ()=>{
    console.log('We are use, ', port)
  });
});

